package com.malsheikh.marvelcharacters.repository.api;



import com.malsheikh.marvelcharacters.repository.charactercomics.model.CharacterComicsResponse;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.CharactersResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


import static com.malsheikh.marvelcharacters.Constants.API_KEY_REQUEST_PARAM;


public interface MarvelAPIInterface {

    @GET("characters")
    Call<CharactersResponse> getMarvelCaracters(@Query("ts") String ts, @Query("hash") String hash, @Query(API_KEY_REQUEST_PARAM) String apiKey,

                                                @Query("limit") int limit, @Query("offset") int offset);

    @GET("characters/{characterId}/comics")
    Call<CharacterComicsResponse> getMarvelCharacterComics(@Path("characterId") long characterId, @Query("ts") String ts, @Query("hash") String hash, @Query(API_KEY_REQUEST_PARAM) String apiKey);
}
