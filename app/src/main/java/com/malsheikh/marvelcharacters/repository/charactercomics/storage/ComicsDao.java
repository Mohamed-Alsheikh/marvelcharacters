package com.malsheikh.marvelcharacters.repository.charactercomics.storage;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.malsheikh.marvelcharacters.repository.charactercomics.model.Result;

import java.util.List;

@Dao
public interface ComicsDao {

    @Query("SELECT * FROM characterComics WHERE characterId=:characterId")
    LiveData<List<Result>> getMarvel(int characterId);


    //
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertComic(Result result);

}
