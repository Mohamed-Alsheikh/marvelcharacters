package com.malsheikh.marvelcharacters.repository.charactercomics.network;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;


import com.malsheikh.marvelcharacters.Constants;
import com.malsheikh.marvelcharacters.repository.api.MarvelAPIInterface;
import com.malsheikh.marvelcharacters.repository.api.MarvelDBAPIClient;
import com.malsheikh.marvelcharacters.repository.charactercomics.model.CharacterComicsResponse;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.subjects.ReplaySubject;

import static com.malsheikh.marvelcharacters.Constants.API_KEY_PUBLIC;


public class CharacterComicsNetwork {
    private final MutableLiveData<NetworkState> networkState = new MutableLiveData<>();
    private MutableLiveData<List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result>> characterComicsLiveData = new MutableLiveData<>();
    private final MarvelAPIInterface marvelApiService;
    static CharacterComicsNetwork instance;
    private final static String SHA_TYPE = "MD5";
    private String timeStamp;
    private String hashText;
    private int offset = 0;
    private final ReplaySubject<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result> marvelObservable;

    public static CharacterComicsNetwork getInstance() {
        if (instance == null) {
            instance = new CharacterComicsNetwork();
        }
        return instance;
    }

    public ReplaySubject<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result> getcomics() {
        return marvelObservable;
    }

    private CharacterComicsNetwork() {
        marvelApiService = MarvelDBAPIClient.getClient();
        timeStamp = String.valueOf(System.currentTimeMillis());
        String preHashBuffer = timeStamp + Constants.API_KEY_PRIVATE + API_KEY_PUBLIC;
        hashText = getSignatureSHA256(preHashBuffer);
        marvelObservable = ReplaySubject.create();
    }

    private static String getSignatureSHA256(String preHashText) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(preHashText.getBytes());
            byte messageDigest[] = digest.digest();

            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public MutableLiveData<List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result>> getMarvelCharacterComics(int recipeId) {
        getFullRecipeFromApi(recipeId);
        return characterComicsLiveData;

    }

    Call<CharacterComicsResponse> callBack;

    private void getFullRecipeFromApi(int recipeId) {
        networkState.postValue(NetworkState.LOADING);
        callBack = marvelApiService.getMarvelCharacterComics(recipeId, timeStamp, hashText, API_KEY_PUBLIC);
        callBack.enqueue(new Callback<CharacterComicsResponse>() {
            @Override
            public void onResponse(@NonNull Call<CharacterComicsResponse> call, @NonNull Response<CharacterComicsResponse> response) {
                if (response.isSuccessful()) {
                    if (emptyRecipe(response.body())) {
                        networkState.postValue(new NetworkState(NetworkState.Status.SUCCESSWITHMSG, response.message()));
                        characterComicsLiveData.postValue(response.body().getData().getResults());

                    } else {
                        networkState.postValue(NetworkState.LOADED);
                        assert response.body() != null;
                        characterComicsLiveData.postValue(response.body().getData().getResults());
                        for(int x=0;x<response.body().getData().getResults().size();x++){
                            response.body().getData().getResults().get(x).setCharacterId(recipeId);
                            marvelObservable.onNext(response.body().getData().getResults().get(x));
                        }
                        Log.e("API CALL", response.message());
                    }

                } else {
                    Log.e("API CALL", response.message());
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CharacterComicsResponse> call, @NonNull Throwable t) {
                String errorMessage;
                if (t.getMessage() == null) {
                    errorMessage = "unknown error";
                } else {
                    errorMessage = t.getMessage();
                }
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));

            }
        });

    }

    private boolean emptyRecipe(CharacterComicsResponse body) {
        return (body != null ? body.getData() : null) == null;
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public void cancelCall() {
        if (callBack != null && callBack.isExecuted()) {
            callBack.cancel();
        }
    }
}
