package com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.paging;

import android.arch.paging.DataSource;

import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.MarvelDao;


public class DBMarvelDataSourceFactory extends DataSource.Factory {

    private static final String TAG = DBMarvelDataSourceFactory.class.getSimpleName();
    private DBMarvelPageKeyedDataSource marvelPageKeyedDataSource;
    public DBMarvelDataSourceFactory(MarvelDao dao) {
        marvelPageKeyedDataSource = new DBMarvelPageKeyedDataSource(dao);
    }

    @Override
    public DataSource create() {
        return marvelPageKeyedDataSource;
    }

}
