package com.malsheikh.marvelcharacters.repository.characterlistrepository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.paging.PagedList;
import android.content.Context;


import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.MarvelCharacterNetwork;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging.MarvelDataSourceFactory;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.MarvelDatabase;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import rx.schedulers.Schedulers;

public class MarvelCharacterRepository {
    private static final String TAG = MarvelCharacterRepository.class.getSimpleName();
    private static MarvelCharacterRepository instance;
    final private MarvelCharacterNetwork network;
    final private MarvelDatabase database;
    final private MediatorLiveData liveDataMerger;
    public MarvelDataSourceFactory dataSourceFactory;

    private MarvelCharacterRepository(Context context) {

        dataSourceFactory = new MarvelDataSourceFactory();
        network = new MarvelCharacterNetwork(dataSourceFactory, boundaryCallback);
        database = MarvelDatabase.getInstance(context.getApplicationContext());
        // when we get new Marvel result from net we set them into the database
        liveDataMerger = new MediatorLiveData<>();


        // save the movies into db
        dataSourceFactory.getMarvel().
                observeOn(Schedulers.io()).
                subscribe(result -> {
                    database.marvelDao().insertMovie(result);
                });

    }

    private PagedList.BoundaryCallback<Result> boundaryCallback = new PagedList.BoundaryCallback<Result>() {
        @Override
        public void onZeroItemsLoaded() {
            super.onZeroItemsLoaded();

            liveDataMerger.addSource(database.getmarvel(), value -> {
                liveDataMerger.setValue(value);
                liveDataMerger.removeSource(database.getmarvel());
            });
        }
    };

    public static MarvelCharacterRepository getInstance(Context context) {
        if (instance == null) {
            instance = new MarvelCharacterRepository(context);
        }
        return instance;
    }

    public LiveData<PagedList<Result>> getMarvel() {
        liveDataMerger.addSource(network.getPagedmarvel(), value -> {
            liveDataMerger.setValue(value);
            liveDataMerger.removeSource(network.getPagedmarvel());
//            Log.d(TAG, value.toString());
        });
        return liveDataMerger;
    }

    public LiveData<NetworkState> getNetworkState() {
        return network.getNetworkState();
    }

    public LiveData<Result> getMarvelCharacterComics(int characterId) {
        return null;
    }
}
