
package com.malsheikh.marvelcharacters.repository.charactercomics.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TextObject implements Parcelable
{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("text")
    @Expose
    private String text;
    public final static Creator<TextObject> CREATOR = new Creator<TextObject>() {


        @SuppressWarnings({
            "unchecked"
        })
        public TextObject createFromParcel(Parcel in) {
            return new TextObject(in);
        }

        public TextObject[] newArray(int size) {
            return (new TextObject[size]);
        }

    }
    ;

    protected TextObject(Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.language = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TextObject() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(language);
        dest.writeValue(text);
    }

    public int describeContents() {
        return  0;
    }

}
