package com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.malsheikh.marvelcharacters.Constants;
import com.malsheikh.marvelcharacters.repository.api.MarvelAPIInterface;
import com.malsheikh.marvelcharacters.repository.api.MarvelDBAPIClient;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.CharactersResponse;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.subjects.ReplaySubject;

import static com.malsheikh.marvelcharacters.Constants.API_KEY_PUBLIC;


/**
 * Responsible for loading the data by page
 */

public class MarvelPageKeyedDataSource extends PageKeyedDataSource<String, Result> {

    private static final String TAG = MarvelPageKeyedDataSource.class.getSimpleName();
    private final MarvelAPIInterface marvelService;
    private final MutableLiveData<NetworkState> networkState;
    private final ReplaySubject<Result> marvelObservable;
    private final static String SHA_TYPE = "MD5";
    private String timeStamp;
    private String hashText;
    private int offset = 0;

    public MarvelPageKeyedDataSource() {
        marvelService = MarvelDBAPIClient.getClient();
        networkState = new MutableLiveData<>();
        marvelObservable = ReplaySubject.create();
        timeStamp = String.valueOf(System.currentTimeMillis());
        String preHashBuffer = timeStamp + Constants.API_KEY_PRIVATE +
                API_KEY_PUBLIC;

        hashText = getSignatureSHA256(preHashBuffer);
    }

    private static String getSignatureSHA256(String preHashText) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(preHashText.getBytes());
            byte messageDigest[] = digest.digest();

            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public ReplaySubject<Result> getMovies() {
        return marvelObservable;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, Result> callback) {
        Log.i(TAG, "Loading Initial Rang, Count " + params.requestedLoadSize);

        networkState.postValue(NetworkState.LOADING);

        Call<CharactersResponse> callBack = marvelService.getMarvelCaracters(timeStamp, hashText, API_KEY_PUBLIC, 20, offset);
        callBack.enqueue(new Callback<CharactersResponse>() {
            @Override
            public void onResponse(Call<CharactersResponse> call, Response<CharactersResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        callback.onResult(response.body().getData().getResults(), Integer.toString(1), Integer.toString(2));
                        networkState.postValue(NetworkState.LOADED);
                        offset = offset + 20;
                        Log.e("offset in load initial", " "+offset+"");
                        for (int x = 0; x < response.body().getData().getResults().size(); x++) {
                            Result result = response.body().getData().getResults().get(x);
                            marvelObservable.onNext(result);
                        }
                    } else {
                        callback.onResult(new ArrayList<Result>(), Integer.toString(1), Integer.toString(2));
                        networkState.postValue(new NetworkState(NetworkState.Status.SUCCESSWITHMSG, "No data to show"));
                    }

                    Log.e("API CALL", response.message());
                } else {
                    Log.e("API CALL", response.message());
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CharactersResponse> call, @NonNull Throwable t) {
                String errorMessage;
                if (t.getMessage() == null) {
                    errorMessage = "unknown error";
                } else {
                    errorMessage = t.getMessage();
                }
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                callback.onResult(new ArrayList<Result>(), Integer.toString(1), Integer.toString(2));
            }
        });
    }


    @Override
    public void loadAfter(@NonNull LoadParams<String> params, final @NonNull LoadCallback<String, Result> callback) {
        Log.i(TAG, "Loading page " + params.key);
        networkState.postValue(NetworkState.LOADING);
        final AtomicInteger page = new AtomicInteger(0);
        try {
            page.set(Integer.parseInt(params.key));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Call<CharactersResponse> callBack = marvelService.getMarvelCaracters(timeStamp, hashText, API_KEY_PUBLIC, 20, offset);
        callBack.enqueue(new Callback<CharactersResponse>() {
            @Override
            public void onResponse(Call<CharactersResponse> call, Response<CharactersResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getData() != null) {
                        callback.onResult(response.body().getData().getResults(), Integer.toString(page.get() + 1));
                        networkState.postValue(NetworkState.LOADED);
                        offset = offset + 20;
                        Log.e("offset in load after", " "+offset+"");
                        for (int x = 0; x < response.body().getData().getResults().size(); x++) {
                            Result result = response.body().getData().getResults().get(x);
                            marvelObservable.onNext(result);
                        }
                    } else {
                        callback.onResult(new ArrayList<Result>(), Integer.toString(page.get() + 1));
                        networkState.postValue(new NetworkState(NetworkState.Status.SUCCESSWITHMSG, "No data to show"));
                    }


                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                    Log.e("API CALL", response.message());
                }
            }

            @Override
            public void onFailure(Call<CharactersResponse> call, Throwable t) {
                String errorMessage;
                if (t.getMessage() == null) {
                    errorMessage = "unknown error";
                } else {
                    errorMessage = t.getMessage();
                }
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                callback.onResult(new ArrayList<Result>(), Integer.toString(page.get()));
            }
        });
    }



    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, Result> callback) {

    }
}
