
package com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.malsheikh.marvelcharacters.utils.thumbnailConverter;

import java.util.List;

@Entity(tableName = "character")
public class Result implements Parcelable {

    @PrimaryKey()
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    private Integer id;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    private String name;

    @ColumnInfo(name = "description")
    @SerializedName("description")
    @Expose
    private String description;


    @ColumnInfo(name = "modified")
    @SerializedName("modified")
    @Expose
    private String modified;


    @ColumnInfo(name = "strthumbnail")
    @SerializedName("strthumbnail")
    @Expose
    private String strthumbnail;


    @TypeConverters({thumbnailConverter.class})
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;

    @Ignore
    @SerializedName("resourceURI")
    @Expose
    private String resourceURI;

    @Ignore
    @SerializedName("comics")
    @Expose
    private Comics comics;

    @Ignore
    @SerializedName("series")
    @Expose
    private Series series;

    @Ignore
    @SerializedName("stories")
    @Expose
    private Stories stories;

    @Ignore
    @SerializedName("events")
    @Expose
    private Events events;

    @Ignore
    @SerializedName("urls")
    @Expose
    private List<Url> urls = null;
    public final static Creator<Result> CREATOR = new Creator<Result>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return (new Result[size]);
        }

    };

    protected Result(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.modified = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbnail = ((Thumbnail) in.readValue((Thumbnail.class.getClassLoader())));
        this.resourceURI = ((String) in.readValue((String.class.getClassLoader())));
        this.comics = ((Comics) in.readValue((Comics.class.getClassLoader())));
        this.series = ((Series) in.readValue((Series.class.getClassLoader())));
        this.stories = ((Stories) in.readValue((Stories.class.getClassLoader())));
        this.events = ((Events) in.readValue((Events.class.getClassLoader())));
        in.readList(this.urls, (Url.class.getClassLoader()));
    }

    public Result(int id, String name, String description, String strthumbnail) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.strthumbnail = strthumbnail;
    }

    // use for ordering the items in view
    public static DiffUtil.ItemCallback<Result> DIFF_CALLBACK = new DiffUtil.ItemCallback<Result>() {
        @Override
        public boolean areItemsTheSame(@NonNull Result oldItem, @NonNull Result newItem) {
            return oldItem.getName() == newItem.getName();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Result oldItem, @NonNull Result newItem) {
            return oldItem.getName() == newItem.getName();
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public Comics getComics() {
        return comics;
    }

    public void setComics(Comics comics) {
        this.comics = comics;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public Stories getStories() {
        return stories;
    }

    public void setStories(Stories stories) {
        this.stories = stories;
    }

    public Events getEvents() {
        return events;
    }

    public void setEvents(Events events) {
        this.events = events;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }

    public String getStrthumbnail() {
        return strthumbnail;
    }

    public void setStrthumbnail(String strthumbnail) {
        this.strthumbnail = strthumbnail;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(description);
        dest.writeValue(modified);
        dest.writeValue(thumbnail);
        dest.writeValue(resourceURI);
        dest.writeValue(comics);
        dest.writeValue(series);
        dest.writeValue(stories);
        dest.writeValue(events);
        dest.writeList(urls);
    }

    public int describeContents() {
        return 0;
    }

}
