package com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.paging;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;


import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.MarvelDao;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import java.util.List;


public class DBMarvelPageKeyedDataSource extends PageKeyedDataSource<String, Result> {

    public static final String TAG = DBMarvelPageKeyedDataSource.class.getSimpleName();
    private final MarvelDao marvelDao;
    public DBMarvelPageKeyedDataSource(MarvelDao dao) {
        marvelDao = dao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, Result> callback) {
        Log.i(TAG, "Loading Initial Rang, Count " + params.requestedLoadSize);
        List<Result> marvel = marvelDao.getMarvel();
        if(marvel.size() != 0) {
            callback.onResult(marvel, "0", "1");
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, final @NonNull LoadCallback<String, Result> callback) {
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, Result> callback) {
    }
}
