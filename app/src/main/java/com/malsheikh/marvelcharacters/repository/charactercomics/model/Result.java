
package com.malsheikh.marvelcharacters.repository.charactercomics.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.malsheikh.marvelcharacters.utils.thumbnailConverterx;

import java.util.List;

@Entity(tableName = "characterComics")
public class Result implements Parcelable {

    @PrimaryKey()
    @ColumnInfo(name = "id")
    @SerializedName("id")
    @Expose
    private Integer id;


    @ColumnInfo(name = "characterId")
    @SerializedName("characterId")
    @Expose
    private Integer characterId;

    @SerializedName("digitalId")
    @Expose
    private Integer digitalId;
    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("issueNumber")
    @Expose
    private Integer issueNumber;
    @SerializedName("variantDescription")
    @Expose
    private String variantDescription;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("isbn")
    @Expose
    private String isbn;
    @SerializedName("upc")
    @Expose
    private String upc;
    @SerializedName("diamondCode")
    @Expose
    private String diamondCode;
    @SerializedName("ean")
    @Expose
    private String ean;
    @SerializedName("issn")
    @Expose
    private String issn;
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;

    @Ignore
    @SerializedName("textObjects")
    @Expose
    private List<TextObject> textObjects = null;

    @Ignore
    @SerializedName("resourceURI")
    @Expose
    private String resourceURI;

    @Ignore
    @SerializedName("urls")
    @Expose
    private List<Url> urls = null;

    @Ignore
    @SerializedName("series")
    @Expose
    private Series series;

    @Ignore
    @SerializedName("variants")
    @Expose
    private List<Object> variants = null;

    @Ignore
    @SerializedName("collections")
    @Expose
    private List<Object> collections = null;

    @Ignore
    @SerializedName("collectedIssues")
    @Expose
    private List<Object> collectedIssues = null;

    @Ignore
    @SerializedName("dates")
    @Expose
    private List<Date> dates = null;

    //*******************************************************
    @Ignore
    @SerializedName("prices")
    @Expose
    private List<Price> prices = null;

//    @Ignore
    @TypeConverters({thumbnailConverterx.class})
    @SerializedName("thumbnail")
    @Expose
    private Thumbnail thumbnail;
    //*******************************************************
    @Ignore
    @SerializedName("images")
    @Expose
    private List<Image> images = null;

    @Ignore
    @SerializedName("creators")
    @Expose
    private Creators creators;

    @Ignore
    @SerializedName("characters")
    @Expose
    private Characters characters;

    @Ignore
    @SerializedName("stories")
    @Expose
    private Stories stories;

    @Ignore
    @SerializedName("events")
    @Expose
    private Events events;
    public final static Creator<Result> CREATOR = new Creator<Result>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return (new Result[size]);
        }

    };

    protected Result(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.digitalId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.issueNumber = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.variantDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.modified = ((String) in.readValue((String.class.getClassLoader())));
        this.isbn = ((String) in.readValue((String.class.getClassLoader())));
        this.upc = ((String) in.readValue((String.class.getClassLoader())));
        this.diamondCode = ((String) in.readValue((String.class.getClassLoader())));
        this.ean = ((String) in.readValue((String.class.getClassLoader())));
        this.issn = ((String) in.readValue((String.class.getClassLoader())));
        this.format = ((String) in.readValue((String.class.getClassLoader())));
        this.pageCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.textObjects, (TextObject.class.getClassLoader()));
        this.resourceURI = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.urls, (Url.class.getClassLoader()));
        this.series = ((Series) in.readValue((Series.class.getClassLoader())));
        in.readList(this.variants, (Object.class.getClassLoader()));
        in.readList(this.collections, (Object.class.getClassLoader()));
        in.readList(this.collectedIssues, (Object.class.getClassLoader()));
        in.readList(this.dates, (Date.class.getClassLoader()));
        in.readList(this.prices, (Price.class.getClassLoader()));
        this.thumbnail = ((Thumbnail) in.readValue((Thumbnail.class.getClassLoader())));
        in.readList(this.images, (Image.class.getClassLoader()));
        this.creators = ((Creators) in.readValue((Creators.class.getClassLoader())));
        this.characters = ((Characters) in.readValue((Characters.class.getClassLoader())));
        this.stories = ((Stories) in.readValue((Stories.class.getClassLoader())));
        this.events = ((Events) in.readValue((Events.class.getClassLoader())));
    }

    public Result() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public Integer getDigitalId() {
        return digitalId;
    }

    public void setDigitalId(Integer digitalId) {
        this.digitalId = digitalId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(Integer issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getVariantDescription() {
        return variantDescription;
    }

    public void setVariantDescription(String variantDescription) {
        this.variantDescription = variantDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getDiamondCode() {
        return diamondCode;
    }

    public void setDiamondCode(String diamondCode) {
        this.diamondCode = diamondCode;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public List<TextObject> getTextObjects() {
        return textObjects;
    }

    public void setTextObjects(List<TextObject> textObjects) {
        this.textObjects = textObjects;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public List<Object> getVariants() {
        return variants;
    }

    public void setVariants(List<Object> variants) {
        this.variants = variants;
    }

    public List<Object> getCollections() {
        return collections;
    }

    public void setCollections(List<Object> collections) {
        this.collections = collections;
    }

    public List<Object> getCollectedIssues() {
        return collectedIssues;
    }

    public void setCollectedIssues(List<Object> collectedIssues) {
        this.collectedIssues = collectedIssues;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Creators getCreators() {
        return creators;
    }

    public void setCreators(Creators creators) {
        this.creators = creators;
    }

    public Characters getCharacters() {
        return characters;
    }

    public void setCharacters(Characters characters) {
        this.characters = characters;
    }

    public Stories getStories() {
        return stories;
    }

    public void setStories(Stories stories) {
        this.stories = stories;
    }

    public Events getEvents() {
        return events;
    }

    public void setEvents(Events events) {
        this.events = events;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(digitalId);
        dest.writeValue(title);
        dest.writeValue(issueNumber);
        dest.writeValue(variantDescription);
        dest.writeValue(description);
        dest.writeValue(modified);
        dest.writeValue(isbn);
        dest.writeValue(upc);
        dest.writeValue(diamondCode);
        dest.writeValue(ean);
        dest.writeValue(issn);
        dest.writeValue(format);
        dest.writeValue(pageCount);
        dest.writeList(textObjects);
        dest.writeValue(resourceURI);
        dest.writeList(urls);
        dest.writeValue(series);
        dest.writeList(variants);
        dest.writeList(collections);
        dest.writeList(collectedIssues);
        dest.writeList(dates);
        dest.writeList(prices);
        dest.writeValue(thumbnail);
        dest.writeList(images);
        dest.writeValue(creators);
        dest.writeValue(characters);
        dest.writeValue(stories);
        dest.writeValue(events);
    }

    public int describeContents() {
        return 0;
    }

}
