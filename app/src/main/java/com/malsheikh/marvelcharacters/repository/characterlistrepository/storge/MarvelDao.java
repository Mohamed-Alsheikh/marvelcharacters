package com.malsheikh.marvelcharacters.repository.characterlistrepository.storge;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import java.util.List;


@Dao
public interface MarvelDao {


    @Query("SELECT * FROM character ORDER BY name ASC")
    List<Result> getMarvel();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMovie(Result result);

    @Query("DELETE FROM character")
    abstract void deleteAllMarvel();
}
