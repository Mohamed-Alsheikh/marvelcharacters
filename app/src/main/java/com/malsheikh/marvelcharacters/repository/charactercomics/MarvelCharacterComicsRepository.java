package com.malsheikh.marvelcharacters.repository.charactercomics;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.malsheikh.marvelcharacters.Constants;
import com.malsheikh.marvelcharacters.repository.charactercomics.model.Result;
import com.malsheikh.marvelcharacters.repository.charactercomics.network.CharacterComicsNetwork;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.MarvelDatabase;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;

import java.util.List;

import rx.schedulers.Schedulers;


public class MarvelCharacterComicsRepository {
    private static MarvelCharacterComicsRepository instance;
    private CharacterComicsNetwork characterComicsNetwork;
    MarvelDatabase database;
    Context context;

    private MarvelCharacterComicsRepository(Context context) {
        this.context = context;
        characterComicsNetwork = CharacterComicsNetwork.getInstance();
        database = MarvelDatabase.getInstance(context.getApplicationContext());
        characterComicsNetwork.getcomics().observeOn(Schedulers.io()).
                subscribe(result -> {
                    database.comicsDao().insertComic(result);
                });
    }

    public static MarvelCharacterComicsRepository getInstance(Context context) {
        if (instance == null) {
            instance = new MarvelCharacterComicsRepository(context);
        }
        return instance;
    }

    public LiveData<List<Result>> getMarvelCharacterComics(int characterId) {
        if (Constants.isNetworkAvailable(context)) {
            return characterComicsNetwork.getMarvelCharacterComics(characterId);
        } else {
            return database.comicsDao().getMarvel(characterId);
        }
    }

    public LiveData<NetworkState> getNetworkState() {
        return characterComicsNetwork.getNetworkState();
    }

    public void cancelCall() {
        characterComicsNetwork.cancelCall();
    }


}
