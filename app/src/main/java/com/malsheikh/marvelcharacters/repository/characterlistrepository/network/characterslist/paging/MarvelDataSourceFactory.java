package com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;


import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import rx.subjects.ReplaySubject;

/*
    Responsible for creating the DataSource so we can give it to the PagedList.
 */
public class MarvelDataSourceFactory extends DataSource.Factory {

    private static final String TAG = MarvelDataSourceFactory.class.getSimpleName();
    public MutableLiveData<MarvelPageKeyedDataSource> networkStatus;
    public MarvelPageKeyedDataSource moviesPageKeyedDataSource;

    public MarvelDataSourceFactory() {
        this.networkStatus = new MutableLiveData<>();
        moviesPageKeyedDataSource = new MarvelPageKeyedDataSource();
    }


    @Override
    public DataSource create() {
        networkStatus.postValue(moviesPageKeyedDataSource);
        return moviesPageKeyedDataSource;
    }

    public MutableLiveData<MarvelPageKeyedDataSource> getNetworkStatus() {
        return networkStatus;
    }

    public ReplaySubject<Result> getMarvel() {
        return moviesPageKeyedDataSource.getMovies();
    }

}
