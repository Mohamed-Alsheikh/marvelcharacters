package com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;


import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging.MarvelDataSourceFactory;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging.MarvelPageKeyedDataSource;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.malsheikh.marvelcharacters.Constants.LOADING_PAGE_SIZE;
import static com.malsheikh.marvelcharacters.Constants.NUMBERS_OF_THREADS;


public class MarvelCharacterNetwork {

    final private static String TAG = MarvelCharacterNetwork.class.getSimpleName();

     private LiveData<NetworkState> networkState;


    private MarvelDataSourceFactory dataSourceFactory;
    private PagedList.BoundaryCallback<Result> boundaryCallback;

    public MarvelCharacterNetwork(MarvelDataSourceFactory dataSourceFactory, PagedList.BoundaryCallback<Result> boundaryCallback){
        this.dataSourceFactory = dataSourceFactory;
        this.boundaryCallback = boundaryCallback;
        networkState = Transformations.switchMap(dataSourceFactory.getNetworkStatus(),
                MarvelPageKeyedDataSource::getNetworkState);

    }


    public LiveData<PagedList<Result>> getPagedmarvel(){
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(LOADING_PAGE_SIZE).setPageSize(LOADING_PAGE_SIZE).build();

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder(dataSourceFactory, pagedListConfig);

        return (LiveData<PagedList<Result>>) livePagedListBuilder.
                setFetchExecutor(executor).
                setBoundaryCallback(boundaryCallback).
                build();

    }



    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

}
