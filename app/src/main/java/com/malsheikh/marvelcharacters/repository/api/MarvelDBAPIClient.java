package com.malsheikh.marvelcharacters.repository.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.malsheikh.marvelcharacters.Constants.POPULAR_MARVEL_BASE_URL;


public class MarvelDBAPIClient {

    public static MarvelAPIInterface getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // create OkHttpClient and register an interceptor
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(POPULAR_MARVEL_BASE_URL);

        return builder.build().create(MarvelAPIInterface.class);
    }
}
