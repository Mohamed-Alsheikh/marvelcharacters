package com.malsheikh.marvelcharacters.repository.characterlistrepository.storge;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;


import com.malsheikh.marvelcharacters.repository.charactercomics.storage.ComicsDao;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.paging.DBMarvelDataSourceFactory;
import com.malsheikh.marvelcharacters.utils.thumbnailConverter;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.malsheikh.marvelcharacters.Constants.DATA_BASE_NAME;
import static com.malsheikh.marvelcharacters.Constants.NUMBERS_OF_THREADS;


/**
 * The Room database that contains the Result table
 */
@Database(entities = {Result.class, com.malsheikh.marvelcharacters.repository.charactercomics.model.Result.class}, version = 2)
@TypeConverters({thumbnailConverter.class, thumbnailConverter.class})
public abstract class MarvelDatabase extends RoomDatabase {

    private static MarvelDatabase instance;

    public abstract MarvelDao marvelDao();

    public abstract ComicsDao comicsDao();

    private static final Object sLock = new Object();
    private LiveData<PagedList<Result>> marvelPaged;

    public static MarvelDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (instance == null) {
                instance = Room.databaseBuilder(context.getApplicationContext(),
                        MarvelDatabase.class, DATA_BASE_NAME)
                        .build();
                instance.init();

            }
            return instance;
        }
    }

    private void init() {
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(Integer.MAX_VALUE).setPageSize(Integer.MAX_VALUE).build();
        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        DBMarvelDataSourceFactory dataSourceFactory = new DBMarvelDataSourceFactory(marvelDao());
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder(dataSourceFactory, pagedListConfig);
        marvelPaged = livePagedListBuilder.setFetchExecutor(executor).build();
    }

    public LiveData<PagedList<Result>> getmarvel() {
        return marvelPaged;
    }
}
