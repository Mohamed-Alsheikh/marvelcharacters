package com.malsheikh.marvelcharacters.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.repository.charactercomics.model.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MarvelCharacterComicsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Result> courseCategoryModel;

    public MarvelCharacterComicsAdapter(List<Result> courseCategoryModel) {

        this.courseCategoryModel = courseCategoryModel;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comic_card, parent, false);
        viewHolder = new MyViewHolder(view);

        return viewHolder;

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder1, final int position) {

        final MyViewHolder holder = (MyViewHolder) holder1;
        final Result courseCategoryModel = this.courseCategoryModel.get(position);
        holder.comic_name.setText(courseCategoryModel.getTitle());

        String poster = courseCategoryModel.getThumbnail().getPath() + "." + courseCategoryModel.getThumbnail().getExtension();
        Picasso.get().load(poster).into(holder.comic_image);


    }


    @Override
    public int getItemCount() {
        return courseCategoryModel.size();
    }


 


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(Result r) {
        if (r != null) {
            courseCategoryModel.add(r);
        }
        notifyDataSetChanged();
    }

    public void addAll(List<Result> moveResults) {

        for (int x = 0; x < moveResults.size(); x++) {
            add(moveResults.get(x));
        }
    }

    public void remove(Result r) {
        int position = courseCategoryModel.indexOf(r);
        if (position > -1) {
            courseCategoryModel.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {

        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public Result getItem(int position) {

        return courseCategoryModel.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView comic_name;

        public ImageView comic_image;


        public MyViewHolder(View view) {
            super(view);

            comic_name = (TextView) view.findViewById(R.id.cat_text);
            comic_image = (ImageView) view.findViewById(R.id.cat_image);

        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}