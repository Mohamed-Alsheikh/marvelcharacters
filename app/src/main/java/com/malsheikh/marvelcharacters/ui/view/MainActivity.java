package com.malsheikh.marvelcharacters.ui.view;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.utils.BackPressed;


public class MainActivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            return;
        }
        addRecipeSearchFragment();
    }

    private void addRecipeSearchFragment() {
        MarvelCharacterListFragment marvelCharacterListFragment = MarvelCharacterListFragment.newInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmenttransaction = fragmentManager.beginTransaction();
        fragmenttransaction.addToBackStack(MarvelCharacterListFragment.class.getName());
        fragmenttransaction.replace(R.id.fragmentsContainer, marvelCharacterListFragment, MarvelCharacterListFragment.class.getName());
        fragmenttransaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (count <= 1) {
            checkBackDoubleClick();
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 2) {
            stopCallIfItInProcessingState();
        } else
            super.onBackPressed();
    }

    private void checkBackDoubleClick() {
        if (doubleBackToExitPressedOnce) {
            finish();
            System.exit(0);
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click Back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void stopCallIfItInProcessingState() {
        Fragment currentFragment = getSupportFragmentManager().getFragments().get(0);
        if (currentFragment instanceof BackPressed) {
            ((BackPressed) currentFragment).onFragmentBackPressedCloseApiCall();
            super.onBackPressed();

        }
    }

}
