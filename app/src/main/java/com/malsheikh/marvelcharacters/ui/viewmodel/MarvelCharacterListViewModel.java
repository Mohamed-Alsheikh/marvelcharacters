package com.malsheikh.marvelcharacters.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import com.malsheikh.marvelcharacters.repository.characterlistrepository.MarvelCharacterRepository;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.AbsentLiveData;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.network.characterslist.paging.MarvelPageKeyedDataSource;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;


public class MarvelCharacterListViewModel extends AndroidViewModel {
    private MarvelCharacterRepository repository;

    private final LiveData<PagedList<Result>> searchResults;
    private final MutableLiveData<Boolean> query = new MutableLiveData<>();

    public MarvelCharacterListViewModel(@NonNull Application application) {
        super(application);
        repository = MarvelCharacterRepository.getInstance(application);
        searchResults = Transformations.switchMap(query, search -> {

                return repository.getMarvel();


        });
    }

    public void startGetMarvelCharacters(boolean refresh) {

        if (refresh) {
            repository.dataSourceFactory.moviesPageKeyedDataSource = new MarvelPageKeyedDataSource();
        }
        query.setValue(true);

    }

    public LiveData<PagedList<Result>> getMarvelCharacters() {
        return searchResults;
    }

    public LiveData<NetworkState> getNetworkState() {
        return repository.getNetworkState();
    }

}
