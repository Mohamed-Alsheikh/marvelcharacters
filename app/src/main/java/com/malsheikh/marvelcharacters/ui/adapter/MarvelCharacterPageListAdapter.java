package com.malsheikh.marvelcharacters.ui.adapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;
import com.malsheikh.marvelcharacters.ui.listeners.ItemClickListener;
import com.malsheikh.marvelcharacters.ui.view.viewholder.MarvelCharacterViewHolder;
import com.malsheikh.marvelcharacters.ui.view.viewholder.NetworkStateItemViewHolder;


public class MarvelCharacterPageListAdapter extends PagedListAdapter<Result, RecyclerView.ViewHolder> {

    private NetworkState networkState;
    private ItemClickListener itemClickListener;

    public MarvelCharacterPageListAdapter(ItemClickListener itemClickListener) {
        super(Result.DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == R.layout.marvel_character_item) {
            View view = layoutInflater.inflate(R.layout.marvel_character_item, parent, false);
            MarvelCharacterViewHolder viewHolder = new MarvelCharacterViewHolder(view, itemClickListener);
            return viewHolder;
        } else if (viewType == R.layout.network_state_item) {
            View view = layoutInflater.inflate(R.layout.network_state_item, parent, false);
            return new NetworkStateItemViewHolder(view);
        } else {
            throw new IllegalArgumentException("unknown view type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case R.layout.marvel_character_item:
                ((MarvelCharacterViewHolder) holder).bindTo(getItem(position));
                break;
            case R.layout.network_state_item:
                ((NetworkStateItemViewHolder) holder).bindView(networkState);
                break;
        }
    }

    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return R.layout.network_state_item;
        } else {
            return R.layout.marvel_character_item;
        }
    }

    public void setNetworkState(NetworkState newNetworkState, Context context) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }
}
