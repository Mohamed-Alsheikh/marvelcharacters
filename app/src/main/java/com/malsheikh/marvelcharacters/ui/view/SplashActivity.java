package com.malsheikh.marvelcharacters.ui.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.malsheikh.marvelcharacters.R;


public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 1000;


    ImageView imgLogo;
    Animation zoomAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imgLogo = (ImageView) findViewById(R.id.imgLogo);
//        imgLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.zoom));
        zoomAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.puls);
        zoomAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {

                finish();
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);

            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        startAnimation();
    }

    void startAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                imgLogo.startAnimation(zoomAnimation);
//                imgLogo.animate()
//                        .alpha(0.0f)
//                        .setDuration(SPLASH_TIME_OUT)
//                        .setListener(new AnimatorListenerAdapter() {
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                super.onAnimationEnd(animation);
//
//                            }
//                        });
            }
        }, SPLASH_TIME_OUT);
    }
}