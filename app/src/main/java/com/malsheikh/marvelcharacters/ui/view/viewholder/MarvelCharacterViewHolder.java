package com.malsheikh.marvelcharacters.ui.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;
import com.malsheikh.marvelcharacters.ui.listeners.ItemClickListener;
import com.squareup.picasso.Picasso;


public class MarvelCharacterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    private Result result;
    private TextView titleTextView;
    private ImageView thumbnailImageView;
    private ItemClickListener itemClickListener;

    public MarvelCharacterViewHolder(View view, ItemClickListener itemClickListener) {
        super(view);
        this.titleTextView = view.findViewById(R.id.title);

        this.thumbnailImageView = view.findViewById(R.id.thumbnail);
        this.itemClickListener = itemClickListener;
        view.setOnClickListener(this);

    }

    public void bindTo(Result result) {
        this.result = result;
        titleTextView.setText(result.getName());

        if(result.getThumbnail() != null) {
            String poster = result.getThumbnail().getPath() +"."+ result.getThumbnail().getExtension();
            Picasso.get().load(poster).into(thumbnailImageView);
        }
    }

    @Override
    public void onClick(View view) {
        if (itemClickListener != null) {
            itemClickListener.OnItemClick(result); // call the onClick in the OnItemClickListener
        }
    }

}
