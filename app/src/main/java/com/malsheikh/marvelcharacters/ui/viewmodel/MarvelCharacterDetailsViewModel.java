package com.malsheikh.marvelcharacters.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.malsheikh.marvelcharacters.repository.charactercomics.MarvelCharacterComicsRepository;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

import java.util.List;


public class MarvelCharacterDetailsViewModel extends AndroidViewModel {
    final private MutableLiveData result;
    private MarvelCharacterComicsRepository repository;
    public final MutableLiveData<Integer> characterId = new MutableLiveData<>();
    private final LiveData<List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result>> searchResults;

    public MarvelCharacterDetailsViewModel(Application application) {
        super(application);
        repository = MarvelCharacterComicsRepository.getInstance(application);
        result = new MutableLiveData<Result>();
        searchResults = Transformations.switchMap(characterId, recipeId -> {
            return repository.getMarvelCharacterComics(recipeId);
        });
    }


    public MutableLiveData<Result> getMarvelCharacter() {
        return result;
    }

    public LiveData<List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result>> getCharacterComics() {
        return searchResults;
    }

    public void setCharacterId(@NonNull int characterId) {
        this.characterId.setValue(characterId);

    }

    public LiveData<NetworkState> getNetworkState() {
        return repository.getNetworkState();
    }


    public void cancelcall() {
        repository.cancelCall();
    }
}
