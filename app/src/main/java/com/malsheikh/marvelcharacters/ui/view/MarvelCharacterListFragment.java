package com.malsheikh.marvelcharacters.ui.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.databinding.FragmentMarvelCharacterBinding;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;
import com.malsheikh.marvelcharacters.ui.adapter.MarvelCharacterPageListAdapter;
import com.malsheikh.marvelcharacters.ui.listeners.ItemClickListener;
import com.malsheikh.marvelcharacters.ui.viewmodel.MarvelCharacterDetailsViewModel;
import com.malsheikh.marvelcharacters.ui.viewmodel.MarvelCharacterListViewModel;


public class MarvelCharacterListFragment extends Fragment implements ItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    View view;
    protected MarvelCharacterListViewModel viewModel;
    private MarvelCharacterDetailsViewModel detailsViewModel;

    final MarvelCharacterPageListAdapter pageListAdapter = new MarvelCharacterPageListAdapter(this);
    protected RecyclerView recyclerView;
    FragmentMarvelCharacterBinding binding;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RecipeSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarvelCharacterListFragment newInstance() {
        MarvelCharacterListFragment fragment = new MarvelCharacterListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_marvel_character, container, false);
            view = binding.getRoot();
            recyclerView = view.findViewById(R.id.recipeRecyclerView);
            binding.toolbar.setLogo(R.drawable.marvel_comics_vector_logo);
//
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            binding.swiperefresh.setOnRefreshListener(this);
            viewModel = ViewModelProviders.of(getActivity()).get(MarvelCharacterListViewModel.class);
            observersRegisters();
            if (savedInstanceState == null) {
                viewModel.startGetMarvelCharacters(false);
            }
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void observersRegisters() {

        detailsViewModel = ViewModelProviders.of(getActivity()).get(MarvelCharacterDetailsViewModel.class);
        viewModel.getMarvelCharacters().observe(this, pageListAdapter::submitList);
//        viewModel.getNetworkState().observe(this, networkState -> {
//            pageListAdapter.setNetworkState(networkState);
//        });
        viewModel.getNetworkState().observe(this, this::handleNetworkStateResponse);
        recyclerView.setAdapter(pageListAdapter);

    }

    private void handleNetworkStateResponse(NetworkState networkState) {
        pageListAdapter.setNetworkState(networkState, getContext());
        switch (networkState.getStatus()) {
            case SUCCESS:
                loadingStateSuccess();
                if (binding.swiperefresh.isRefreshing()) {
                    binding.swiperefresh.setRefreshing(false);
                }

                break;
            case SUCCESSWITHMSG:
                LoadingStateSucessWithMessage(networkState.getMsg());
                if (binding.swiperefresh.isRefreshing()) {
                    binding.swiperefresh.setRefreshing(false);
                }
                break;
            case FAILED:
                loadingStateFailed(networkState.getMsg());
                if (binding.swiperefresh.isRefreshing()) {
                    binding.swiperefresh.setRefreshing(false);
                }
                break;
            case RUNNING:
                if (viewModel.getMarvelCharacters().getValue() != null && viewModel.getMarvelCharacters().getValue().size() == 0) {
                    loadingStateRunning();
                }
        }
    }

    private void loadingStateRunning() {
        binding.loading.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.errorMsg.setVisibility(View.GONE);
    }

    private void loadingStateFailed(String msg) {
        binding.loading.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorMsg.setVisibility(View.VISIBLE);
        if (viewModel.getMarvelCharacters().getValue() != null && viewModel.getMarvelCharacters().getValue().size() == 0) {
            binding.errorMsg.setText(msg+"\n Swipe down to refresh");
        }

        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

    }

    private void loadingStateSuccess() {
        binding.loading.setVisibility(View.GONE);
    }

    private void LoadingStateSucessWithMessage(String msg) {
        binding.loading.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorMsg.setVisibility(View.VISIBLE);
//        binding.errorMsg.setText(msg);
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void OnItemClick(Result result) {
        detailsViewModel.getMarvelCharacter().postValue(result);
        detailsViewModel.setCharacterId(result.getId());
        if (!detailsViewModel.getMarvelCharacter().hasActiveObservers()) {
            // Create fragment and give it an argument specifying the article it should show
            MarvelCharacterDetailsFragment detailsFragment = new MarvelCharacterDetailsFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragmentsContainer, detailsFragment);
            transaction.addToBackStack(MarvelCharacterDetailsFragment.class.getName());
            // Commit the transaction
            transaction.commit();
        }

    }

    @Override
    public void onRefresh() {

        if (alreadyRunning()) {
            Toast.makeText(getContext(), "already running", Toast.LENGTH_SHORT).show();
            binding.swiperefresh.setRefreshing(false);
        } else {
            viewModel.startGetMarvelCharacters(true);
        }

    }

    private boolean alreadyRunning() {
        return viewModel.getNetworkState().getValue() != null && viewModel.getNetworkState().getValue().getStatus() == NetworkState.Status.RUNNING;
    }

}
