package com.malsheikh.marvelcharacters.ui.view;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.malsheikh.marvelcharacters.Constants;
import com.malsheikh.marvelcharacters.R;
import com.malsheikh.marvelcharacters.databinding.FragmentMarvelCharacterDetailsBinding;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.NetworkState;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;
import com.malsheikh.marvelcharacters.ui.adapter.MarvelCharacterComicsAdapter;
import com.malsheikh.marvelcharacters.ui.viewmodel.MarvelCharacterDetailsViewModel;
import com.malsheikh.marvelcharacters.utils.BackPressed;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MarvelCharacterDetailsFragment extends Fragment implements BackPressed {
    FragmentMarvelCharacterDetailsBinding binding;
    private MarvelCharacterDetailsViewModel viewModel;
    ArrayList<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result> coursesCategoriesArrayList = new ArrayList<>();
    MarvelCharacterComicsAdapter marvelCharacterComicsAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate view and obtain an instance of the binding class.
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_marvel_character_details, container, false);

        View view = binding.getRoot();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        binding.comicsRecyclerview.setNestedScrollingEnabled(false);
        binding.comicsRecyclerview.setLayoutManager(layoutManager);
        marvelCharacterComicsAdapter = new MarvelCharacterComicsAdapter(coursesCategoriesArrayList);
        binding.comicsRecyclerview.setAdapter(marvelCharacterComicsAdapter);

        binding.toolbar.setNavigationIcon(R.drawable.ic_chevron_left_black_24dp);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        binding.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isNetworkAvailable(getContext())) {
                    viewModel.setCharacterId(viewModel.characterId.getValue());
                } else
                    Toast.makeText(getContext(), "No Internet access", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }


    private void handleNetworkStateResponse(NetworkState networkState) {

        switch (networkState.getStatus()) {
            case SUCCESS:
                loadingStateSuccess();

                break;
            case SUCCESSWITHMSG:
                LoadingStateSucessWithMessage(networkState.getMsg());

                break;
            case FAILED:
                loadingStateFailed(networkState.getMsg());
                break;

            case RUNNING:
                loadingStateRunning();

        }
    }

    private void loadingStateRunning() {
        binding.refresh.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.errorMsg.setVisibility(View.GONE);
        marvelCharacterComicsAdapter.clear();
    }

    private void loadingStateFailed(String msg) {
        binding.refresh.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorMsg.setVisibility(View.VISIBLE);
        binding.errorMsg.setText(msg);
    }

    private void loadingStateSuccess() {
        binding.progressBar.setVisibility(View.GONE);
        binding.errorMsg.setVisibility(View.GONE);
        binding.refresh.setVisibility(View.GONE);
    }

    private void LoadingStateSucessWithMessage(String msg) {

        binding.progressBar.setVisibility(View.GONE);
        binding.refresh.setVisibility(View.GONE);
        binding.errorMsg.setVisibility(View.VISIBLE);
        binding.errorMsg.setText(msg);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(MarvelCharacterDetailsViewModel.class);
        observRegisters();


    }

    private void observRegisters() {
        viewModel.getMarvelCharacter().observe(this, this::getcharacter);
        viewModel.getCharacterComics().observe(this, this::setComics);
        viewModel.getNetworkState().observe(this, this::handleNetworkStateResponse);
    }

    private void getcharacter(Result result) {
        setSubDetails(result);
    }

    private void setComics(List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result> characterComicsResponse) {
        marvelCharacterComicsAdapter.clear();
        marvelCharacterComicsAdapter.addAll(characterComicsResponse);
        if (Constants.isNetworkAvailable(getContext())) {
            validateUiinDifferentNetworkStatus(characterComicsResponse, " no data to show");
        } else {
            validateUiinDifferentNetworkStatus(characterComicsResponse, "No internet access and no data to show");

        }

    }

    private void validateUiinDifferentNetworkStatus(List<com.malsheikh.marvelcharacters.repository.charactercomics.model.Result> characterComicsResponse, String msg) {
        if (characterComicsResponse.size() == 0) {
            binding.progressBar.setVisibility(View.GONE);
            binding.errorMsg.setText(msg);
            binding.errorMsg.setVisibility(View.VISIBLE);
            binding.refresh.setVisibility(View.VISIBLE);
        } else {
            binding.progressBar.setVisibility(View.GONE);
            binding.errorMsg.setVisibility(View.GONE);
            binding.refresh.setVisibility(View.GONE);
        }
    }


    private void setSubDetails(Result result) {
        binding.characterTitle.setText(result.getName());
        binding.desc.setText(result.getDescription());
        if (result.getThumbnail() != null) {
            String poster = result.getThumbnail().getPath() + "." + result.getThumbnail().getExtension();
            Picasso.get().load(poster).into(binding.charactermage);
        }
    }

    @Override
    public void onFragmentBackPressedCloseApiCall() {
        if (isRunning()) {
            viewModel.cancelcall();
        }
    }


    private boolean isRunning() {
        return viewModel.getNetworkState().getValue() != null && viewModel.getNetworkState().getValue().getStatus() == NetworkState.Status.RUNNING;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

