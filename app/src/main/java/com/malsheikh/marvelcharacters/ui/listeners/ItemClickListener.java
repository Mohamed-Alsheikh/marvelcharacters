package com.malsheikh.marvelcharacters.ui.listeners;


import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Result;

public interface ItemClickListener {
    void OnItemClick(Result result);
}
