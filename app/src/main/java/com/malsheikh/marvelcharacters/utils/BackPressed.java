package com.malsheikh.marvelcharacters.utils;

public interface BackPressed {

    void onFragmentBackPressedCloseApiCall();
}
