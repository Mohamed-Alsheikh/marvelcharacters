package com.malsheikh.marvelcharacters.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.malsheikh.marvelcharacters.repository.characterlistrepository.storge.model.Thumbnail;

public class thumbnailConverter {
    @TypeConverter
    public static String toString(Thumbnail thumbnail) {

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(thumbnail);
        return json;
    }

    @TypeConverter
    public static Thumbnail toString(String thumbnail) {

        Gson gson = new GsonBuilder().create();
        Thumbnail thumbnail1 = gson.fromJson(thumbnail, Thumbnail.class);
        return thumbnail1;
    }
}
