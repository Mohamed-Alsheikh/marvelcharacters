package com.malsheikh.marvelcharacters;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Objects;

public class Constants {
    // network

    public static final String POPULAR_MARVEL_BASE_URL = "https://gateway.marvel.com:443/v1/public/";
    public static final String API_KEY_PUBLIC = "ee474fc8159c1b7d582473331ea35a27";
    public static final String API_KEY_PRIVATE = "ed4d551cbda56aab0ce02c5fd681931d21b6278a";


    public static final String API_KEY_REQUEST_PARAM = "apikey";

    public static final String PAGE_REQUEST_PARAM = "page";
    public static final String API_KEY = "04f2f288263683f12131ae2ae1d348c6";
    public static final String LANGUAGE = "en";
    public static final int LOADING_PAGE_SIZE = 10;
    // DB
    public static final String DATA_BASE_NAME = "CharacterListDB.db";
    public static final int NUMBERS_OF_THREADS = 3;
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
