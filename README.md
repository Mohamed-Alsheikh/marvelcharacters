# Marvel

Marvel is android app where user can see Marvel Characters and see details of Character and List of Comics.

this App developed based on android [Architecture Components MVVM](https://developer.android.com/arch):
![mvvm](https://cdn-images-1.medium.com/max/1024/1*CEW0JrVST7ReQq04j0P0kw.png)
![mvvm](https://cdn-images-1.medium.com/max/1200/0*29EMJcTrYcRSn9rW.jpg)
- [Android Architecture Components][arch]
- [Room](https://developer.android.com/topic/libraries/architecture/room)
- [Lifecycle-aware components](https://developer.android.com/topic/libraries/architecture/lifecycle)
- [ViewModels](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [Paging](https://developer.android.com/topic/libraries/architecture/paging/)
- [Android Support Library][support-lib]
- [Retrofit][retrofit] for REST api communication
- [OkHttp][OkHttp] for adding interceptors to Retrofit
- [Picasso][Picasso] for image loading
- [RxJava](https://github.com/ReactiveX/RxJava) 
[support-lib]: https://developer.android.com/topic/libraries/support-library/index.html
[arch]: https://developer.android.com/arch
[OkHttp]: http://square.github.io/okhttp/
[retrofit]: http://square.github.io/retrofit
[picasso]: http://square.github.io/picasso/

### Requirements

* JDK Version 1.8 & above
* Android Studio Preview Version 3.0
* minimum SDK version 16

### Prerequisites
For the app to make requests you require a [public API key,Private API key](https://developer.marvel.com/account ).

If you don’t already have an account, you will need to [create one](https://www.marvel.com/signin?referer=https%3A%2F%2Fdeveloper.marvel.com%2Faccount)
in order to request an API Key.

Once you have it, open `Constant.java` file and paste your API key in `API_KEY` variable.
Authentication [Authentication](https://developer.marvel.com/documentation/authorization)
Authentication for Server-Side Applications
Server-side applications must pass two parameters in addition to the apikey parameter:

ts - a timestamp (or other long string which can change on a request-by-request basis)
hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)
For example, a user with a public key of "1234" and a private key of "abcd" could construct a valid call as follows: http://gateway.marvel.com/v1/public/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150 (the hash value is the md5 digest of 1abcd1234)




